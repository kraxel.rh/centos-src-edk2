##
#    SPDX-License-Identifier: BSD-2-Clause-Patent
##

!if $(BUILD_SHELL) == TRUE && $(SECURE_BOOT_ENABLE) == FALSE

!if $(TOOL_CHAIN_TAG) != "XCODE5"
INF  ShellPkg/DynamicCommand/VariablePolicyDynamicCommand/VariablePolicyDynamicCommand.inf
!endif

INF  ShellPkg/Application/Shell/Shell.inf
!endif
